# Project Viewer Development Status 


## Version
- v0.7: Nearly functional, see "Latest August 25" notes
- v0.8: New version ... i'm almost certainly giving up on v0.7 & just developing this version to goodness
- v0.9: Abandoning v0.8 to do a complete rewrite with a much simpler design

## Ideas
- seo
    - canonical urls (`project/dir` point to `project/dir/`)
    - `og:` meta tags
    - project image (for `og:image` tag)
- add tag viewing
- add commit hash viewing
- link to git repo page
- tabbed home page for a project 
    - readme
    - install
    - dependencies
    - api
    - source code
    - money

- theme integration (easy way to replace the home page for all projects, or for aaa project, or to add pages)

## Issues / TODO
- most of my tests don't have pass conditions
- `project/.git` will show the abs path to the git work tree. The .git file/dir should probably ALWAYS be hidden & inaccessible bc it's not actually project info.


# March 30, 2022
doing work in `code/gitlab-hook.php` and `test/run/GitlabHook.php`. More recent work in the test file & is really just notes

## March 29, 2022
Worked on the gitlab webhook. Not much code. Good notes though. See the `code/public/gitlab-hook.php` file ...

also, need to integrate the Liaison integration from taeluf.com code into this code.

I'm very excited to have a working way to run a script after my php script is done executing. I tried prematurely disconnecting my php before and never was able to get that to work. I suppose I could try again... but i also don't want anything intensive running during the script & holding up apache or whatever

Also see `test/input/webhook-gitlab.json`... it has a sample push from gitlab.

I do need a master file listing all projects with their descriptions, so i can load the projects page

I could also maybe use phad ... hmmm

## March 25, 2022
- added basic implementation of get_default_branch using a `settings.json` file in the `projects` dir.
- added a second project (liaison) And it's working well

## March 14, 2022
It works! I wrote a bunch of tests, to test url parsing as well as several individual views. I filled out breadcrumbs & fixed the views to work with the new code. I added a Project class, mainly as an enum for the result of parse_url.

After writing the tests for the Project class / parse_url, I added some new properties to it & I have not updated those tests.

I wrote the view tests but none of them are technically passing. I believe they are ALL passing, but none of them have valid comparisons going on.

So my tests all need some TLC to show that they are passing. The code itself doesn't.

There may be some bugs & edge cases:
- files with weird extensions (mime type not found)
- multiple slashes in the url
- urls inside documentation (for one example, there are images in a README file & the images don't work because the path is relative, but the readme file is inside `0-docs` folder ... i think this is v0.1)


### So what's next?
- integrate git webhooks 
    - i previously implemented them for github ... i can probably use that code again
    - but i need gitlab webhooks ... idk how i'll do all this
- Clean up old, bad code
- properly integrate Autowire
- make all tests pass
- add a way to do very easy setup (like a single line kinda thing)
- cleanup my notes here (put old notes in a devlog.md file, read over them)
- integrate hljs (php backend)
- install on taeluf.com

### Eventually?
- maybe improve some styles? idk
    - currently, i think I'm relying upon my site's styles for code blocks & hljs & stuff
- search for files/dirs by name
- improve performance (caching & stuff)





## Jan 29, 2022 end of day
so i'm getting confused because i'm passing a `$url` around, but before i pass that url around, i'm removing the base url ... that's dumb ... i need a better way to specify which part of the url is the viewer's prefix, the branch's prefix, and the relative file route ... need to review `Provi` to clean all that up

Further, i need deliniation between different paths ... i have the path to the project's base dir, then the path to the active branch's dir, then the relative path to the docs dir inside the branch's dir, then the path to the file inside the branch dir & the path to the active directory inside the branch dir

... then sidebar is a mess ... i have some notes in there


I also think things are just kind of confusing and out of order? Idunno ... i like what i'm doing with the new provi code, but i'm getting a little lost & a little confused & i need to take a little thoughtful time to review that code & clean it up ...

once i have it cleaned up & all the url parsing + deriving information from the project/branch ... then i should be able to get the views displaying real easy


## Jan 29, 2022
I am doing a rewrite. I've added the ability to download all branches of a git repo. I've started a new `Provi` class. I'm building a `get_response()` function (which is wrapped by a `route()` function to make some things easier ... maybe `get_response` should be renamed bc it actually returns a view, not a full response) ... I need to continue in `get_response` and parse the request url & then figure out how to route after parsing that url

If freshly setting up this repo, then run
`../../../code/getrepo.bash git@gitlab.com:taeluf/php/liaison-app/project-viewer.git Provi`
from `test/Server/projects`


### Keeping local copies of all branches
Basically:
1. `git clone --mirror git@gitlab.com:taeluf/php/liaison-app/project-viewer.git Provi/.git`
2. `cd Provi/.git`
3. `git worktree add ../v0.1 v0.1` ...

This is implemented in `code/getrepo.bash`


## Jan 27, 2021
use `phptest server` to run the server
`/taeluf/provi` shows the readme 
viewing a directory fails
viewing other files works
viewing code files seems to work, but `LICENSE` doesn't for some reason
source code viewing works
branch switching popup works ... but i don't know about actual branch switching ...


Honestly, i kind of think i just need to spend an hour or two completely redesigning how this works ... I don't think it's good for me to keep trying to make the old code work well ... it's not going to because it's terrible code. The views are mostly decent ... but the classes are horrible

and in this process, i should update it to use the new liaison

as per ush these days, i want the liaison integration to be as minimal as possible
i want this to ... essentially just have a routing function that returns headers & content (or maybe just sends headers for raw files ... or maybe diff routing for that? bc like images, i could show on a page & have you click a 'raw file' link)

Then the liaison integration should basically just set up a router function that wraps Provi's routing with Liaison's route & request objects

oh, and i want to be writing tests! unit tests, then integration tests


-- notes from a little earlier


this project is HORRIBLE. i hate the routing. I think I need to rewrite it from scratch and just super de-complicate things

I'm having issues with the sidebar ... i want it to show all the files from the root, with certain nodes expanded ... but it's only showing for the current directory ... its also showing relative paths, not just names

I don't like how the projects are setup ... i want it to be a git clone & me do ZERO moving around of files ... i just want to give it a dir for a project & have it work
... or i could give it a parent dir ... where each sub-directory is a project (so i don't have to add projects manually)

I don't want to programmatically do anything other than give a file path to the project. Projects should have configs in them if their structures don't match the defaults/derived for project viewer.

if there cannot be configs in the project dirs, then there should be other configs that detail whatever is necessary

worst case scenario, i just don't know the documentation directory, and that's not that big of a deal

the project list isn't displaying

it's tempting to use phad, though that seems like overkill for this

the design used to look sexy & now it does not ... am i missing some css files?




## Latest (August 25, 2021)
still use `bin/provi` to launch server
Visiting `/taeluf/provi` delivers successfully.
- Autowire is now being delivered
Theme needs some actual styling (v minor) & general cleanup (since i copied it from taeluf.com)
Can't view any specific files or directories
can switch between docs & src code



## Latest (August 24, 2021)
Now we use `bin/provi` (short for `project viewer`). 

Visit localhost:port/taeluf/project-viewer/

I got the `ProjectLayout` view mostly cleaned up & now I need to work through `Sidebar` view. Last error cites line 6 of `view/Docu/Sidebar.php:6` as trying to call `Router::filesFor()`.

This method will likely move to the project. I may change the implementation of the dir for a project. Reason being that I want one place of logic for making paths. In `filesFor()` I don't want to check if we're currently in `view source` mode or `view docs` mode. I want to just scan `dir/relPath`.

That being said, I may want `dir/relPath` to be stored on the project. But then I'm kind of mixing the project information up with the state of the request. The current implementation is more about the request. A project itself might be better off just being in charge of telling you default branch & stuff, but not have any awareness of state. Idk.

## Latest (August 23, 2021)
To use execute `bin/view` in the terminal while inside `Project-Viewer` dir (so we can view docs for Project Viewer), then go to the `localhost:port` that it tells you to. Visit the three urls:
- `/`
- `/taeluf/` (vendor url)
- `/taeluf/project-viewer/`

additional urls to try later:
- `/taeluf/project-viewer/some_file.md` (do I include `.md` in the docs viewer? i don't think so. Probably in the src viewer)
- `/taeluf/project-viewer-src/some_file.php`
- `/taeluf/project-viewer/some/dir/`
- `/taeluf/project-viewer:branch_name/`
- `/taeluf/project-viewer-src/`
- `/taeluf/project-viewer-src:branch_name/`

- I moved old code to `code/old-core`
- I started a new `Router` class that parses the url into its information pieces
- I scaffolded the call to load in the `Docu/ProjectLayout` view.
- NEXT: `ProjectLayout` needs to be reviewed, and the associated implementation for projects & stuff needs to be figured out so this can happen.
    - Probably review the router & see if I want to have that flow any differently, including making a project object from the info?
    - I probably don't want the router to answer "isDocsRequest" because the router doesn't maintain a state & I don't really want it to. The project can maintain state, or something. Idk.

## Current
- Just created `v0.1` branch from `webhooks_rewrite-for-multi-vendors`. I'm pretty sure I can delete everything else. BUT, I might convert a couple of them to v0.X branches & bump this one up to like v0.4 or something.
- JUST copied in `webhook-stuff-from-taeluf.com`... might need this later
- Just started `code-new/` folder to put forth a new design. I am just having a really hard time understanding what the current code is actually doing, so I'm just looking at a new way to go about things.
- Just moved all old branches to v0.X scheme and now I will work on v0.7



- Started work on cli interface & new architecture
- Started on trying to clean up everything & its kind of a mess right now. I'm working out a dynamic url that will allow me to catch a request. I updated liaison to take `-` & `:` as separators of dynamic bits. I need the optional one `?`. But most of all, I have to get away from all these complicated function calls. I need a single `parseUrl` that returns vendor name, project name, and so on. Then I can use the parsed info to check if the project & file actually exist here.
- I don't know what I'm doing about the class structure around projects & vendors. Idk what doing about the git status. 
- Currently, I'm focusing on just delivering a single directory as a project. Then I want to go from there & start figuring out these more complex issues. By focusing on the simple part, I can create some good architecture & it will be easy to move forward.
- I feel like I want to delete most of my old code, but that's not quite it. There's just places where I'm chaining together many function calls with bad names & I don't understand how things work. The interface for routing, loading projects, parsing urls, updating projects (git pulls), and all need to be... so so much better.

## How to run
```bash
cd server.php
php -S localhost:3000 deliver.php
```

## Plans
- Make it work on any folder of code without any configuration
- Scan for files & wherever md files are concentrated its documentation city
- Cli install instructions & a basic cli interface
    - Setup a new project on your server to receive webhooks
    - view the current folder as project via webserver
    - 
- Composer
- Integrate gitlab webhooks
- Refactor all code 

## Next
- Delete all old, unneeded code
- Make the server runnable for `pwd`, so you can view documentation of any arbitrary folder on your system without any setup required
- refactor all existing code to... make some sense!
- Get webhooks working again (with gitlab this time! But github, too)
- Fill out a built-in-theme so it actually looks pretty
- Write documentation
    - How do I set up a new project?
- Write tests

## Next (later)
- Support multiple vendors
- idk

## Latest (newest to oldest)
- Sample server to view documentation
- It... works. I can view files & navigate, but there's still lots of issues.
- Nothing to report yet...
