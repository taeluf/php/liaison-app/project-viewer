<div class="BranchPopup" style="display:none">
    <div class="BranchDialog">
        <button class="DialogX">X</button>
        <h6>Choose a Branch</h6>
        <div class="info">
            <strong>Default:</strong> <?=$project->default_branch?>
            <br>
            <strong>Current:</strong> <?=$project->branch_name?>
        </div>
        <!-- <br><br> -->
        <nav>
            <div>
            <?php // while(@$i++<10)
            foreach ($project->all_branches() as $url=>$name): 
                if ($name==$project->branch)$selected='class="selected"';
                else $selected = '';
            ?>
                <a <?=$selected?> href="<?=$url?>"><?=$name?></a>
            <?php endforeach; ?>
            </div>
        </nav>
    </div>
</div>
