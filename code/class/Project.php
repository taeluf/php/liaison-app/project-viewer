<?php

namespace Tlf\Provi;

//enum
//project isn't the right name ...
class Project {

    /** the url prefix (generally not specific to the project */
    public $prefix;
    /** the full url to the project's home page (including the current branch and type) */
    public $project_url;
    /** the branch */
    public $branch;
    /** the name of the project */
    public $project_name;
    /** The type of the request: docs, src, project_listing */
    public $type;
    /** absolute path to the branch's directory on disk */
    public $branch_dir;
    /** default branch name */
    public $default_branch;
    /** absolute path to the docs dir */
    public $docs_dir;
    /** relative path to the file being requested. Path within the requested dir (either docs dir or branch dir */
    public $rel_file_path;
    /** relative path to the dir being requested. either dirname($rel_file_path) or $rel_file_path if it is already a directory request */
    public $rel_dir_path;
    /** absolute path to the requested file */
    public $abs_file_path;
    /** absolute path to the requested directory (either dirname($abs_file_path) or $abs_file_path if it is a dir) */
    public $abs_dir_path;

    /** true/false whether current branch is default branch or not */
    public $is_default_branch;

    /** full url to view source for the current project & branch */
    public function src_url(){
        $branch = $this->is_default_branch ? '' : ':'.$this->branch;
        return $this->prefix.'/'.$this->project_name.'-src'.$branch.'/';
    }

    /** full url to view docs for the current project & branch */
    public function docs_url(){
        $branch = $this->is_default_branch ? '' : ':'.$this->branch;
        return $this->prefix.'/'.$this->project_name.$branch.'/';
    }

    /** alias for $project_name */
    public function name(){
        return $this->project_name;
    }
    /** alias for $branch */
    public function branch_name(){return $this->branch;}

    /** absolute path to the current base directory ('branch dir' or 'branch dir / docs', basically */
    public function current_base_dir(){
        if ($this->type=='docs')return $this->docs_dir;
        return $this->branch_dir;
    }

    /**
     * The project url + the rel_file_path
     */
    public function current_url(){
        return $this->project_url.'/'.$this->rel_file_path;
    }

    /** alias for rel_file_path */
    public function current_rel_url(){
        return '/'.$this->rel_file_path;
        // $current = $this->current_url;
        // $rel = substr($current,strlen($this->prefix));
        // $rel = '/'.$rel;
        // return $rel;
    }


    public function __get($prop){
        return str_replace('//', '/', $this->$prop());
    }


    /**
     * get array of all branches
     */
    public function all_branches(){
        $project_dir = dirname($this->branch_dir);
        $branches = [];
        foreach (scandir($project_dir) as $f){
            if ($f=='.'||$f=='..'||$f=='.git')continue;
            $url = $this->prefix.'/'.$this->project_name.':'.$f;
            $url = str_replace('//', '/',$url);
            $branches[$url] = $f;
        }
        return $branches;
    }
}
