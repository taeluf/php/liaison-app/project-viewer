<?php

ob_start();

try {

$rootDir = dirname(dirname(dirname(__DIR__)));

$wh = new \tlf\GithubWebhook(
    $_POST['payload'], 
    '~/.ssh/github_taeluf_webhook',
    $rootDir.'/7-work/webhooks/',
    $rootDir.'/6-Wiki/',
    array_keys($gitRepos),
    [
        'Taeluf'
    ]
);


$wh->outputCurrentBranch();

} catch (\Exception $e){
    echo 'we caught an exception';
} catch (\Throwable $e){
    echo 'we caught a throwable';
} catch (\Error $e){
    echo 'We caught an error';
} finally {
    //so I'm not passing anything sensitive back to github, such as my username on my server!
    ob_get_clean();
}

exit;